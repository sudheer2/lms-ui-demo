import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import VoucherUpdatePage from './voucher-update.page-object';

const expect = chai.expect;
export class VoucherDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucher.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-voucher'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class VoucherComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('voucher-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('voucher');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateVoucher() {
    await this.createButton.click();
    return new VoucherUpdatePage();
  }

  async deleteVoucher() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const voucherDeleteDialog = new VoucherDeleteDialog();
    await waitUntilDisplayed(voucherDeleteDialog.deleteModal);
    expect(await voucherDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.voucher.delete.question/);
    await voucherDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(voucherDeleteDialog.deleteModal);

    expect(await isVisible(voucherDeleteDialog.deleteModal)).to.be.false;
  }
}
