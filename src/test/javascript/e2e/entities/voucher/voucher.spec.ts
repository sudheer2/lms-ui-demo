import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import VoucherComponentsPage from './voucher.page-object';
import VoucherUpdatePage from './voucher-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Voucher e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let voucherComponentsPage: VoucherComponentsPage;
  let voucherUpdatePage: VoucherUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    voucherComponentsPage = new VoucherComponentsPage();
    voucherComponentsPage = await voucherComponentsPage.goToPage(navBarPage);
  });

  it('should load Vouchers', async () => {
    expect(await voucherComponentsPage.title.getText()).to.match(/Vouchers/);
    expect(await voucherComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Vouchers', async () => {
    const beforeRecordsCount = (await isVisible(voucherComponentsPage.noRecords)) ? 0 : await getRecordsCount(voucherComponentsPage.table);
    voucherUpdatePage = await voucherComponentsPage.goToCreateVoucher();
    await voucherUpdatePage.enterData();

    expect(await voucherComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(voucherComponentsPage.table);
    await waitUntilCount(voucherComponentsPage.records, beforeRecordsCount + 1);
    expect(await voucherComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await voucherComponentsPage.deleteVoucher();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(voucherComponentsPage.records, beforeRecordsCount);
      expect(await voucherComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(voucherComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
