import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class VoucherUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucher.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  voucherCodeInput: ElementFinder = element(by.css('input#voucher-voucherCode'));
  voucherNameInput: ElementFinder = element(by.css('input#voucher-voucherName'));
  assignedInput: ElementFinder = element(by.css('input#voucher-assigned'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setVoucherCodeInput(voucherCode) {
    await this.voucherCodeInput.sendKeys(voucherCode);
  }

  async getVoucherCodeInput() {
    return this.voucherCodeInput.getAttribute('value');
  }

  async setVoucherNameInput(voucherName) {
    await this.voucherNameInput.sendKeys(voucherName);
  }

  async getVoucherNameInput() {
    return this.voucherNameInput.getAttribute('value');
  }

  getAssignedInput() {
    return this.assignedInput;
  }
  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setVoucherCodeInput('voucherCode');
    expect(await this.getVoucherCodeInput()).to.match(/voucherCode/);
    await waitUntilDisplayed(this.saveButton);
    await this.setVoucherNameInput('voucherName');
    expect(await this.getVoucherNameInput()).to.match(/voucherName/);
    await waitUntilDisplayed(this.saveButton);
    const selectedAssigned = await this.getAssignedInput().isSelected();
    if (selectedAssigned) {
      await this.getAssignedInput().click();
      expect(await this.getAssignedInput().isSelected()).to.be.false;
    } else {
      await this.getAssignedInput().click();
      expect(await this.getAssignedInput().isSelected()).to.be.true;
    }
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
