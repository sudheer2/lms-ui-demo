import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class VoucherStudentUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherStudent.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  voucherSelect: ElementFinder = element(by.css('select#voucher-student-voucher'));
  laptopStudentSelect: ElementFinder = element(by.css('select#voucher-student-laptopStudent'));

  getPageTitle() {
    return this.pageTitle;
  }

  async voucherSelectLastOption() {
    await this.voucherSelect.all(by.tagName('option')).last().click();
  }

  async voucherSelectOption(option) {
    await this.voucherSelect.sendKeys(option);
  }

  getVoucherSelect() {
    return this.voucherSelect;
  }

  async getVoucherSelectedOption() {
    return this.voucherSelect.element(by.css('option:checked')).getText();
  }

  async laptopStudentSelectLastOption() {
    await this.laptopStudentSelect.all(by.tagName('option')).last().click();
  }

  async laptopStudentSelectOption(option) {
    await this.laptopStudentSelect.sendKeys(option);
  }

  getLaptopStudentSelect() {
    return this.laptopStudentSelect;
  }

  async getLaptopStudentSelectedOption() {
    return this.laptopStudentSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await this.voucherSelectLastOption();
    await this.laptopStudentSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
