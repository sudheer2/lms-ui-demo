import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import VoucherStudentUpdatePage from './voucher-student-update.page-object';

const expect = chai.expect;
export class VoucherStudentDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherStudent.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-voucherStudent'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class VoucherStudentComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('voucher-student-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('voucher-student');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateVoucherStudent() {
    await this.createButton.click();
    return new VoucherStudentUpdatePage();
  }

  async deleteVoucherStudent() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const voucherStudentDeleteDialog = new VoucherStudentDeleteDialog();
    await waitUntilDisplayed(voucherStudentDeleteDialog.deleteModal);
    expect(await voucherStudentDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.voucherStudent.delete.question/);
    await voucherStudentDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(voucherStudentDeleteDialog.deleteModal);

    expect(await isVisible(voucherStudentDeleteDialog.deleteModal)).to.be.false;
  }
}
