import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import VoucherStudentComponentsPage from './voucher-student.page-object';
import VoucherStudentUpdatePage from './voucher-student-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('VoucherStudent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let voucherStudentComponentsPage: VoucherStudentComponentsPage;
  let voucherStudentUpdatePage: VoucherStudentUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    voucherStudentComponentsPage = new VoucherStudentComponentsPage();
    voucherStudentComponentsPage = await voucherStudentComponentsPage.goToPage(navBarPage);
  });

  it('should load VoucherStudents', async () => {
    expect(await voucherStudentComponentsPage.title.getText()).to.match(/Voucher Students/);
    expect(await voucherStudentComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete VoucherStudents', async () => {
    const beforeRecordsCount = (await isVisible(voucherStudentComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(voucherStudentComponentsPage.table);
    voucherStudentUpdatePage = await voucherStudentComponentsPage.goToCreateVoucherStudent();
    await voucherStudentUpdatePage.enterData();

    expect(await voucherStudentComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(voucherStudentComponentsPage.table);
    await waitUntilCount(voucherStudentComponentsPage.records, beforeRecordsCount + 1);
    expect(await voucherStudentComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await voucherStudentComponentsPage.deleteVoucherStudent();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(voucherStudentComponentsPage.records, beforeRecordsCount);
      expect(await voucherStudentComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(voucherStudentComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
