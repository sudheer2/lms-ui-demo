import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class LaptopPickupScheduleUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopPickupSchedule.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  dateInput: ElementFinder = element(by.css('input#laptop-pickup-schedule-date'));
  startHoursInput: ElementFinder = element(by.css('input#laptop-pickup-schedule-startHours'));
  endHoursInput: ElementFinder = element(by.css('input#laptop-pickup-schedule-endHours'));
  programSelect: ElementFinder = element(by.css('select#laptop-pickup-schedule-program'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setDateInput(date) {
    await this.dateInput.sendKeys(date);
  }

  async getDateInput() {
    return this.dateInput.getAttribute('value');
  }

  async setStartHoursInput(startHours) {
    await this.startHoursInput.sendKeys(startHours);
  }

  async getStartHoursInput() {
    return this.startHoursInput.getAttribute('value');
  }

  async setEndHoursInput(endHours) {
    await this.endHoursInput.sendKeys(endHours);
  }

  async getEndHoursInput() {
    return this.endHoursInput.getAttribute('value');
  }

  async programSelectLastOption() {
    await this.programSelect.all(by.tagName('option')).last().click();
  }

  async programSelectOption(option) {
    await this.programSelect.sendKeys(option);
  }

  getProgramSelect() {
    return this.programSelect;
  }

  async getProgramSelectedOption() {
    return this.programSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setDateInput('01-01-2001');
    expect(await this.getDateInput()).to.eq('2001-01-01');
    await waitUntilDisplayed(this.saveButton);
    await this.setStartHoursInput('startHours');
    expect(await this.getStartHoursInput()).to.match(/startHours/);
    await waitUntilDisplayed(this.saveButton);
    await this.setEndHoursInput('endHours');
    expect(await this.getEndHoursInput()).to.match(/endHours/);
    await this.programSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
