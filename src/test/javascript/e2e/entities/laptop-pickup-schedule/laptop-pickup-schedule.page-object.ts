import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import LaptopPickupScheduleUpdatePage from './laptop-pickup-schedule-update.page-object';

const expect = chai.expect;
export class LaptopPickupScheduleDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopPickupSchedule.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-laptopPickupSchedule'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class LaptopPickupScheduleComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('laptop-pickup-schedule-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('laptop-pickup-schedule');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateLaptopPickupSchedule() {
    await this.createButton.click();
    return new LaptopPickupScheduleUpdatePage();
  }

  async deleteLaptopPickupSchedule() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const laptopPickupScheduleDeleteDialog = new LaptopPickupScheduleDeleteDialog();
    await waitUntilDisplayed(laptopPickupScheduleDeleteDialog.deleteModal);
    expect(await laptopPickupScheduleDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /trainsLmsUiApp.laptopPickupSchedule.delete.question/
    );
    await laptopPickupScheduleDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(laptopPickupScheduleDeleteDialog.deleteModal);

    expect(await isVisible(laptopPickupScheduleDeleteDialog.deleteModal)).to.be.false;
  }
}
