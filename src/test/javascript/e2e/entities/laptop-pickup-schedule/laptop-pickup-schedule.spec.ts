import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import LaptopPickupScheduleComponentsPage from './laptop-pickup-schedule.page-object';
import LaptopPickupScheduleUpdatePage from './laptop-pickup-schedule-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('LaptopPickupSchedule e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let laptopPickupScheduleComponentsPage: LaptopPickupScheduleComponentsPage;
  let laptopPickupScheduleUpdatePage: LaptopPickupScheduleUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    laptopPickupScheduleComponentsPage = new LaptopPickupScheduleComponentsPage();
    laptopPickupScheduleComponentsPage = await laptopPickupScheduleComponentsPage.goToPage(navBarPage);
  });

  it('should load LaptopPickupSchedules', async () => {
    expect(await laptopPickupScheduleComponentsPage.title.getText()).to.match(/Laptop Pickup Schedules/);
    expect(await laptopPickupScheduleComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete LaptopPickupSchedules', async () => {
    const beforeRecordsCount = (await isVisible(laptopPickupScheduleComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(laptopPickupScheduleComponentsPage.table);
    laptopPickupScheduleUpdatePage = await laptopPickupScheduleComponentsPage.goToCreateLaptopPickupSchedule();
    await laptopPickupScheduleUpdatePage.enterData();

    expect(await laptopPickupScheduleComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(laptopPickupScheduleComponentsPage.table);
    await waitUntilCount(laptopPickupScheduleComponentsPage.records, beforeRecordsCount + 1);
    expect(await laptopPickupScheduleComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await laptopPickupScheduleComponentsPage.deleteLaptopPickupSchedule();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(laptopPickupScheduleComponentsPage.records, beforeRecordsCount);
      expect(await laptopPickupScheduleComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(laptopPickupScheduleComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
