import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ProgramCoursesComponentsPage from './program-courses.page-object';
import ProgramCoursesUpdatePage from './program-courses-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ProgramCourses e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let programCoursesComponentsPage: ProgramCoursesComponentsPage;
  let programCoursesUpdatePage: ProgramCoursesUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    programCoursesComponentsPage = new ProgramCoursesComponentsPage();
    programCoursesComponentsPage = await programCoursesComponentsPage.goToPage(navBarPage);
  });

  it('should load ProgramCourses', async () => {
    expect(await programCoursesComponentsPage.title.getText()).to.match(/Program Courses/);
    expect(await programCoursesComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ProgramCourses', async () => {
    const beforeRecordsCount = (await isVisible(programCoursesComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(programCoursesComponentsPage.table);
    programCoursesUpdatePage = await programCoursesComponentsPage.goToCreateProgramCourses();
    await programCoursesUpdatePage.enterData();

    expect(await programCoursesComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(programCoursesComponentsPage.table);
    await waitUntilCount(programCoursesComponentsPage.records, beforeRecordsCount + 1);
    expect(await programCoursesComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await programCoursesComponentsPage.deleteProgramCourses();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(programCoursesComponentsPage.records, beforeRecordsCount);
      expect(await programCoursesComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(programCoursesComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
