import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ProgramCoursesUpdatePage from './program-courses-update.page-object';

const expect = chai.expect;
export class ProgramCoursesDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.programCourses.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-programCourses'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ProgramCoursesComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('program-courses-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('program-courses');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateProgramCourses() {
    await this.createButton.click();
    return new ProgramCoursesUpdatePage();
  }

  async deleteProgramCourses() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const programCoursesDeleteDialog = new ProgramCoursesDeleteDialog();
    await waitUntilDisplayed(programCoursesDeleteDialog.deleteModal);
    expect(await programCoursesDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.programCourses.delete.question/);
    await programCoursesDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(programCoursesDeleteDialog.deleteModal);

    expect(await isVisible(programCoursesDeleteDialog.deleteModal)).to.be.false;
  }
}
