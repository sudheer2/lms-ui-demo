import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ProgramCoursesUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.programCourses.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  programSelect: ElementFinder = element(by.css('select#program-courses-program'));
  courseSelect: ElementFinder = element(by.css('select#program-courses-course'));

  getPageTitle() {
    return this.pageTitle;
  }

  async programSelectLastOption() {
    await this.programSelect.all(by.tagName('option')).last().click();
  }

  async programSelectOption(option) {
    await this.programSelect.sendKeys(option);
  }

  getProgramSelect() {
    return this.programSelect;
  }

  async getProgramSelectedOption() {
    return this.programSelect.element(by.css('option:checked')).getText();
  }

  async courseSelectLastOption() {
    await this.courseSelect.all(by.tagName('option')).last().click();
  }

  async courseSelectOption(option) {
    await this.courseSelect.sendKeys(option);
  }

  getCourseSelect() {
    return this.courseSelect;
  }

  async getCourseSelectedOption() {
    return this.courseSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await this.programSelectLastOption();
    await this.courseSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
