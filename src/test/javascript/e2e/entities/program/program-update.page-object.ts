import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ProgramUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.program.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  programNameInput: ElementFinder = element(by.css('input#program-programName'));
  startDateInput: ElementFinder = element(by.css('input#program-startDate'));
  endDateInput: ElementFinder = element(by.css('input#program-endDate'));
  totalSeatsInput: ElementFinder = element(by.css('input#program-totalSeats'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setProgramNameInput(programName) {
    await this.programNameInput.sendKeys(programName);
  }

  async getProgramNameInput() {
    return this.programNameInput.getAttribute('value');
  }

  async setStartDateInput(startDate) {
    await this.startDateInput.sendKeys(startDate);
  }

  async getStartDateInput() {
    return this.startDateInput.getAttribute('value');
  }

  async setEndDateInput(endDate) {
    await this.endDateInput.sendKeys(endDate);
  }

  async getEndDateInput() {
    return this.endDateInput.getAttribute('value');
  }

  async setTotalSeatsInput(totalSeats) {
    await this.totalSeatsInput.sendKeys(totalSeats);
  }

  async getTotalSeatsInput() {
    return this.totalSeatsInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setProgramNameInput('programName');
    expect(await this.getProgramNameInput()).to.match(/programName/);
    await waitUntilDisplayed(this.saveButton);
    await this.setStartDateInput('01-01-2001');
    expect(await this.getStartDateInput()).to.eq('2001-01-01');
    await waitUntilDisplayed(this.saveButton);
    await this.setEndDateInput('01-01-2001');
    expect(await this.getEndDateInput()).to.eq('2001-01-01');
    await waitUntilDisplayed(this.saveButton);
    await this.setTotalSeatsInput('5');
    expect(await this.getTotalSeatsInput()).to.eq('5');
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
