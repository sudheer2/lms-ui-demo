import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ProgramUpdatePage from './program-update.page-object';

const expect = chai.expect;
export class ProgramDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.program.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-program'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ProgramComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('program-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('program');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateProgram() {
    await this.createButton.click();
    return new ProgramUpdatePage();
  }

  async deleteProgram() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const programDeleteDialog = new ProgramDeleteDialog();
    await waitUntilDisplayed(programDeleteDialog.deleteModal);
    expect(await programDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.program.delete.question/);
    await programDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(programDeleteDialog.deleteModal);

    expect(await isVisible(programDeleteDialog.deleteModal)).to.be.false;
  }
}
