import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ProgramComponentsPage from './program.page-object';
import ProgramUpdatePage from './program-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Program e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let programComponentsPage: ProgramComponentsPage;
  let programUpdatePage: ProgramUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    programComponentsPage = new ProgramComponentsPage();
    programComponentsPage = await programComponentsPage.goToPage(navBarPage);
  });

  it('should load Programs', async () => {
    expect(await programComponentsPage.title.getText()).to.match(/Programs/);
    expect(await programComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Programs', async () => {
    const beforeRecordsCount = (await isVisible(programComponentsPage.noRecords)) ? 0 : await getRecordsCount(programComponentsPage.table);
    programUpdatePage = await programComponentsPage.goToCreateProgram();
    await programUpdatePage.enterData();

    expect(await programComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(programComponentsPage.table);
    await waitUntilCount(programComponentsPage.records, beforeRecordsCount + 1);
    expect(await programComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await programComponentsPage.deleteProgram();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(programComponentsPage.records, beforeRecordsCount);
      expect(await programComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(programComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
