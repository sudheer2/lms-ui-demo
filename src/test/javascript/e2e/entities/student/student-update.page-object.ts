import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class StudentUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.student.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  firstNameInput: ElementFinder = element(by.css('input#student-firstName'));
  lastNameInput: ElementFinder = element(by.css('input#student-lastName'));
  cityInput: ElementFinder = element(by.css('input#student-city'));
  stateInput: ElementFinder = element(by.css('input#student-state'));
  countyInput: ElementFinder = element(by.css('input#student-county'));
  zipcodeInput: ElementFinder = element(by.css('input#student-zipcode'));
  emailIdInput: ElementFinder = element(by.css('input#student-emailId'));
  phoneNumberInput: ElementFinder = element(by.css('input#student-phoneNumber'));
  statusSelect: ElementFinder = element(by.css('select#student-status'));
  notesInput: ElementFinder = element(by.css('input#student-notes'));
  leadSourceInput: ElementFinder = element(by.css('input#student-leadSource'));
  courseSelect: ElementFinder = element(by.css('select#student-course'));
  programSelect: ElementFinder = element(by.css('select#student-program'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setFirstNameInput(firstName) {
    await this.firstNameInput.sendKeys(firstName);
  }

  async getFirstNameInput() {
    return this.firstNameInput.getAttribute('value');
  }

  async setLastNameInput(lastName) {
    await this.lastNameInput.sendKeys(lastName);
  }

  async getLastNameInput() {
    return this.lastNameInput.getAttribute('value');
  }

  async setCityInput(city) {
    await this.cityInput.sendKeys(city);
  }

  async getCityInput() {
    return this.cityInput.getAttribute('value');
  }

  async setStateInput(state) {
    await this.stateInput.sendKeys(state);
  }

  async getStateInput() {
    return this.stateInput.getAttribute('value');
  }

  async setCountyInput(county) {
    await this.countyInput.sendKeys(county);
  }

  async getCountyInput() {
    return this.countyInput.getAttribute('value');
  }

  async setZipcodeInput(zipcode) {
    await this.zipcodeInput.sendKeys(zipcode);
  }

  async getZipcodeInput() {
    return this.zipcodeInput.getAttribute('value');
  }

  async setEmailIdInput(emailId) {
    await this.emailIdInput.sendKeys(emailId);
  }

  async getEmailIdInput() {
    return this.emailIdInput.getAttribute('value');
  }

  async setPhoneNumberInput(phoneNumber) {
    await this.phoneNumberInput.sendKeys(phoneNumber);
  }

  async getPhoneNumberInput() {
    return this.phoneNumberInput.getAttribute('value');
  }

  async setStatusSelect(status) {
    await this.statusSelect.sendKeys(status);
  }

  async getStatusSelect() {
    return this.statusSelect.element(by.css('option:checked')).getText();
  }

  async statusSelectLastOption() {
    await this.statusSelect.all(by.tagName('option')).last().click();
  }
  async setNotesInput(notes) {
    await this.notesInput.sendKeys(notes);
  }

  async getNotesInput() {
    return this.notesInput.getAttribute('value');
  }

  async setLeadSourceInput(leadSource) {
    await this.leadSourceInput.sendKeys(leadSource);
  }

  async getLeadSourceInput() {
    return this.leadSourceInput.getAttribute('value');
  }

  async courseSelectLastOption() {
    await this.courseSelect.all(by.tagName('option')).last().click();
  }

  async courseSelectOption(option) {
    await this.courseSelect.sendKeys(option);
  }

  getCourseSelect() {
    return this.courseSelect;
  }

  async getCourseSelectedOption() {
    return this.courseSelect.element(by.css('option:checked')).getText();
  }

  async programSelectLastOption() {
    await this.programSelect.all(by.tagName('option')).last().click();
  }

  async programSelectOption(option) {
    await this.programSelect.sendKeys(option);
  }

  getProgramSelect() {
    return this.programSelect;
  }

  async getProgramSelectedOption() {
    return this.programSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setFirstNameInput('firstName');
    expect(await this.getFirstNameInput()).to.match(/firstName/);
    await waitUntilDisplayed(this.saveButton);
    await this.setLastNameInput('lastName');
    expect(await this.getLastNameInput()).to.match(/lastName/);
    await waitUntilDisplayed(this.saveButton);
    await this.setCityInput('city');
    expect(await this.getCityInput()).to.match(/city/);
    await waitUntilDisplayed(this.saveButton);
    await this.setStateInput('state');
    expect(await this.getStateInput()).to.match(/state/);
    await waitUntilDisplayed(this.saveButton);
    await this.setCountyInput('county');
    expect(await this.getCountyInput()).to.match(/county/);
    await waitUntilDisplayed(this.saveButton);
    await this.setZipcodeInput('zipcode');
    expect(await this.getZipcodeInput()).to.match(/zipcode/);
    await waitUntilDisplayed(this.saveButton);
    await this.setEmailIdInput('emailId');
    expect(await this.getEmailIdInput()).to.match(/emailId/);
    await waitUntilDisplayed(this.saveButton);
    await this.setPhoneNumberInput('phoneNumber');
    expect(await this.getPhoneNumberInput()).to.match(/phoneNumber/);
    await waitUntilDisplayed(this.saveButton);
    await this.statusSelectLastOption();
    await waitUntilDisplayed(this.saveButton);
    await this.setNotesInput('notes');
    expect(await this.getNotesInput()).to.match(/notes/);
    await waitUntilDisplayed(this.saveButton);
    await this.setLeadSourceInput('leadSource');
    expect(await this.getLeadSourceInput()).to.match(/leadSource/);
    await this.courseSelectLastOption();
    await this.programSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
