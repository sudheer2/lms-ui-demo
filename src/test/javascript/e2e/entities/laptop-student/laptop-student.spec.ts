import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import LaptopStudentComponentsPage from './laptop-student.page-object';
import LaptopStudentUpdatePage from './laptop-student-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('LaptopStudent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let laptopStudentComponentsPage: LaptopStudentComponentsPage;
  let laptopStudentUpdatePage: LaptopStudentUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    laptopStudentComponentsPage = new LaptopStudentComponentsPage();
    laptopStudentComponentsPage = await laptopStudentComponentsPage.goToPage(navBarPage);
  });

  it('should load LaptopStudents', async () => {
    expect(await laptopStudentComponentsPage.title.getText()).to.match(/Laptop Students/);
    expect(await laptopStudentComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete LaptopStudents', async () => {
    const beforeRecordsCount = (await isVisible(laptopStudentComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(laptopStudentComponentsPage.table);
    laptopStudentUpdatePage = await laptopStudentComponentsPage.goToCreateLaptopStudent();
    await laptopStudentUpdatePage.enterData();

    expect(await laptopStudentComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(laptopStudentComponentsPage.table);
    await waitUntilCount(laptopStudentComponentsPage.records, beforeRecordsCount + 1);
    expect(await laptopStudentComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await laptopStudentComponentsPage.deleteLaptopStudent();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(laptopStudentComponentsPage.records, beforeRecordsCount);
      expect(await laptopStudentComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(laptopStudentComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
