import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import LaptopStudentUpdatePage from './laptop-student-update.page-object';

const expect = chai.expect;
export class LaptopStudentDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopStudent.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-laptopStudent'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class LaptopStudentComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('laptop-student-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('laptop-student');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateLaptopStudent() {
    await this.createButton.click();
    return new LaptopStudentUpdatePage();
  }

  async deleteLaptopStudent() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const laptopStudentDeleteDialog = new LaptopStudentDeleteDialog();
    await waitUntilDisplayed(laptopStudentDeleteDialog.deleteModal);
    expect(await laptopStudentDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.laptopStudent.delete.question/);
    await laptopStudentDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(laptopStudentDeleteDialog.deleteModal);

    expect(await isVisible(laptopStudentDeleteDialog.deleteModal)).to.be.false;
  }
}
