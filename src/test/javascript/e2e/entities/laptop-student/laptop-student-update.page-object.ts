import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class LaptopStudentUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopStudent.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  studentSelect: ElementFinder = element(by.css('select#laptop-student-student'));
  laptopSelect: ElementFinder = element(by.css('select#laptop-student-laptop'));

  getPageTitle() {
    return this.pageTitle;
  }

  async studentSelectLastOption() {
    await this.studentSelect.all(by.tagName('option')).last().click();
  }

  async studentSelectOption(option) {
    await this.studentSelect.sendKeys(option);
  }

  getStudentSelect() {
    return this.studentSelect;
  }

  async getStudentSelectedOption() {
    return this.studentSelect.element(by.css('option:checked')).getText();
  }

  async laptopSelectLastOption() {
    await this.laptopSelect.all(by.tagName('option')).last().click();
  }

  async laptopSelectOption(option) {
    await this.laptopSelect.sendKeys(option);
  }

  getLaptopSelect() {
    return this.laptopSelect;
  }

  async getLaptopSelectedOption() {
    return this.laptopSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await this.studentSelectLastOption();
    await this.laptopSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
