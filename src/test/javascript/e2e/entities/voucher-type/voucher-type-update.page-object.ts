import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class VoucherTypeUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherType.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  voucherCategorySelect: ElementFinder = element(by.css('select#voucher-type-voucherCategory'));
  voucherSelect: ElementFinder = element(by.css('select#voucher-type-voucher'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setVoucherCategorySelect(voucherCategory) {
    await this.voucherCategorySelect.sendKeys(voucherCategory);
  }

  async getVoucherCategorySelect() {
    return this.voucherCategorySelect.element(by.css('option:checked')).getText();
  }

  async voucherCategorySelectLastOption() {
    await this.voucherCategorySelect.all(by.tagName('option')).last().click();
  }
  async voucherSelectLastOption() {
    await this.voucherSelect.all(by.tagName('option')).last().click();
  }

  async voucherSelectOption(option) {
    await this.voucherSelect.sendKeys(option);
  }

  getVoucherSelect() {
    return this.voucherSelect;
  }

  async getVoucherSelectedOption() {
    return this.voucherSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.voucherCategorySelectLastOption();
    await this.voucherSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
