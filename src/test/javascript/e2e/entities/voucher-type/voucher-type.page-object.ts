import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import VoucherTypeUpdatePage from './voucher-type-update.page-object';

const expect = chai.expect;
export class VoucherTypeDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherType.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-voucherType'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class VoucherTypeComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('voucher-type-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('voucher-type');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateVoucherType() {
    await this.createButton.click();
    return new VoucherTypeUpdatePage();
  }

  async deleteVoucherType() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const voucherTypeDeleteDialog = new VoucherTypeDeleteDialog();
    await waitUntilDisplayed(voucherTypeDeleteDialog.deleteModal);
    expect(await voucherTypeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/trainsLmsUiApp.voucherType.delete.question/);
    await voucherTypeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(voucherTypeDeleteDialog.deleteModal);

    expect(await isVisible(voucherTypeDeleteDialog.deleteModal)).to.be.false;
  }
}
