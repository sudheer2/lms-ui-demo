import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import VoucherTypeComponentsPage from './voucher-type.page-object';
import VoucherTypeUpdatePage from './voucher-type-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('VoucherType e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let voucherTypeComponentsPage: VoucherTypeComponentsPage;
  let voucherTypeUpdatePage: VoucherTypeUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    voucherTypeComponentsPage = new VoucherTypeComponentsPage();
    voucherTypeComponentsPage = await voucherTypeComponentsPage.goToPage(navBarPage);
  });

  it('should load VoucherTypes', async () => {
    expect(await voucherTypeComponentsPage.title.getText()).to.match(/Voucher Types/);
    expect(await voucherTypeComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete VoucherTypes', async () => {
    const beforeRecordsCount = (await isVisible(voucherTypeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(voucherTypeComponentsPage.table);
    voucherTypeUpdatePage = await voucherTypeComponentsPage.goToCreateVoucherType();
    await voucherTypeUpdatePage.enterData();

    expect(await voucherTypeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(voucherTypeComponentsPage.table);
    await waitUntilCount(voucherTypeComponentsPage.records, beforeRecordsCount + 1);
    expect(await voucherTypeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await voucherTypeComponentsPage.deleteVoucherType();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(voucherTypeComponentsPage.records, beforeRecordsCount);
      expect(await voucherTypeComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(voucherTypeComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
