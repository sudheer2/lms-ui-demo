import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import LaptopInventoryUpdatePage from './laptop-inventory-update.page-object';

const expect = chai.expect;
export class LaptopInventoryDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopInventory.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-laptopInventory'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class LaptopInventoryComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('laptop-inventory-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('laptop-inventory');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateLaptopInventory() {
    await this.createButton.click();
    return new LaptopInventoryUpdatePage();
  }

  async deleteLaptopInventory() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const laptopInventoryDeleteDialog = new LaptopInventoryDeleteDialog();
    await waitUntilDisplayed(laptopInventoryDeleteDialog.deleteModal);
    expect(await laptopInventoryDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /trainsLmsUiApp.laptopInventory.delete.question/
    );
    await laptopInventoryDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(laptopInventoryDeleteDialog.deleteModal);

    expect(await isVisible(laptopInventoryDeleteDialog.deleteModal)).to.be.false;
  }
}
