import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class LaptopInventoryUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.laptopInventory.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#laptop-inventory-name'));
  makeSelect: ElementFinder = element(by.css('select#laptop-inventory-make'));
  modelInput: ElementFinder = element(by.css('input#laptop-inventory-model'));
  remoteConnectionIdInput: ElementFinder = element(by.css('input#laptop-inventory-remoteConnectionId'));
  assignedInput: ElementFinder = element(by.css('input#laptop-inventory-assigned'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  async setMakeSelect(make) {
    await this.makeSelect.sendKeys(make);
  }

  async getMakeSelect() {
    return this.makeSelect.element(by.css('option:checked')).getText();
  }

  async makeSelectLastOption() {
    await this.makeSelect.all(by.tagName('option')).last().click();
  }
  async setModelInput(model) {
    await this.modelInput.sendKeys(model);
  }

  async getModelInput() {
    return this.modelInput.getAttribute('value');
  }

  async setRemoteConnectionIdInput(remoteConnectionId) {
    await this.remoteConnectionIdInput.sendKeys(remoteConnectionId);
  }

  async getRemoteConnectionIdInput() {
    return this.remoteConnectionIdInput.getAttribute('value');
  }

  getAssignedInput() {
    return this.assignedInput;
  }
  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setNameInput('name');
    expect(await this.getNameInput()).to.match(/name/);
    await waitUntilDisplayed(this.saveButton);
    await this.makeSelectLastOption();
    await waitUntilDisplayed(this.saveButton);
    await this.setModelInput('model');
    expect(await this.getModelInput()).to.match(/model/);
    await waitUntilDisplayed(this.saveButton);
    await this.setRemoteConnectionIdInput('remoteConnectionId');
    expect(await this.getRemoteConnectionIdInput()).to.match(/remoteConnectionId/);
    await waitUntilDisplayed(this.saveButton);
    const selectedAssigned = await this.getAssignedInput().isSelected();
    if (selectedAssigned) {
      await this.getAssignedInput().click();
      expect(await this.getAssignedInput().isSelected()).to.be.false;
    } else {
      await this.getAssignedInput().click();
      expect(await this.getAssignedInput().isSelected()).to.be.true;
    }
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
