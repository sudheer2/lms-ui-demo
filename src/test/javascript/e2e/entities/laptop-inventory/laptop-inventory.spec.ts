import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import LaptopInventoryComponentsPage from './laptop-inventory.page-object';
import LaptopInventoryUpdatePage from './laptop-inventory-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('LaptopInventory e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let laptopInventoryComponentsPage: LaptopInventoryComponentsPage;
  let laptopInventoryUpdatePage: LaptopInventoryUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    laptopInventoryComponentsPage = new LaptopInventoryComponentsPage();
    laptopInventoryComponentsPage = await laptopInventoryComponentsPage.goToPage(navBarPage);
  });

  it('should load LaptopInventories', async () => {
    expect(await laptopInventoryComponentsPage.title.getText()).to.match(/Laptop Inventories/);
    expect(await laptopInventoryComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete LaptopInventories', async () => {
    const beforeRecordsCount = (await isVisible(laptopInventoryComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(laptopInventoryComponentsPage.table);
    laptopInventoryUpdatePage = await laptopInventoryComponentsPage.goToCreateLaptopInventory();
    await laptopInventoryUpdatePage.enterData();

    expect(await laptopInventoryComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(laptopInventoryComponentsPage.table);
    await waitUntilCount(laptopInventoryComponentsPage.records, beforeRecordsCount + 1);
    expect(await laptopInventoryComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await laptopInventoryComponentsPage.deleteLaptopInventory();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(laptopInventoryComponentsPage.records, beforeRecordsCount);
      expect(await laptopInventoryComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(laptopInventoryComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
