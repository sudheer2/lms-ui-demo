import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class VoucherTypeCourseUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherTypeCourse.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  courseSelect: ElementFinder = element(by.css('select#voucher-type-course-course'));
  voucherSelect: ElementFinder = element(by.css('select#voucher-type-course-voucher'));

  getPageTitle() {
    return this.pageTitle;
  }

  async courseSelectLastOption() {
    await this.courseSelect.all(by.tagName('option')).last().click();
  }

  async courseSelectOption(option) {
    await this.courseSelect.sendKeys(option);
  }

  getCourseSelect() {
    return this.courseSelect;
  }

  async getCourseSelectedOption() {
    return this.courseSelect.element(by.css('option:checked')).getText();
  }

  async voucherSelectLastOption() {
    await this.voucherSelect.all(by.tagName('option')).last().click();
  }

  async voucherSelectOption(option) {
    await this.voucherSelect.sendKeys(option);
  }

  getVoucherSelect() {
    return this.voucherSelect;
  }

  async getVoucherSelectedOption() {
    return this.voucherSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await this.courseSelectLastOption();
    await this.voucherSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
