import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import VoucherTypeCourseUpdatePage from './voucher-type-course-update.page-object';

const expect = chai.expect;
export class VoucherTypeCourseDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('trainsLmsUiApp.voucherTypeCourse.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-voucherTypeCourse'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class VoucherTypeCourseComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('voucher-type-course-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('voucher-type-course');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateVoucherTypeCourse() {
    await this.createButton.click();
    return new VoucherTypeCourseUpdatePage();
  }

  async deleteVoucherTypeCourse() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const voucherTypeCourseDeleteDialog = new VoucherTypeCourseDeleteDialog();
    await waitUntilDisplayed(voucherTypeCourseDeleteDialog.deleteModal);
    expect(await voucherTypeCourseDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /trainsLmsUiApp.voucherTypeCourse.delete.question/
    );
    await voucherTypeCourseDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(voucherTypeCourseDeleteDialog.deleteModal);

    expect(await isVisible(voucherTypeCourseDeleteDialog.deleteModal)).to.be.false;
  }
}
