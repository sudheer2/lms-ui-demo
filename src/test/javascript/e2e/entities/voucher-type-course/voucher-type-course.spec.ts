import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import VoucherTypeCourseComponentsPage from './voucher-type-course.page-object';
import VoucherTypeCourseUpdatePage from './voucher-type-course-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('VoucherTypeCourse e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let voucherTypeCourseComponentsPage: VoucherTypeCourseComponentsPage;
  let voucherTypeCourseUpdatePage: VoucherTypeCourseUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    voucherTypeCourseComponentsPage = new VoucherTypeCourseComponentsPage();
    voucherTypeCourseComponentsPage = await voucherTypeCourseComponentsPage.goToPage(navBarPage);
  });

  it('should load VoucherTypeCourses', async () => {
    expect(await voucherTypeCourseComponentsPage.title.getText()).to.match(/Voucher Type Courses/);
    expect(await voucherTypeCourseComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete VoucherTypeCourses', async () => {
    const beforeRecordsCount = (await isVisible(voucherTypeCourseComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(voucherTypeCourseComponentsPage.table);
    voucherTypeCourseUpdatePage = await voucherTypeCourseComponentsPage.goToCreateVoucherTypeCourse();
    await voucherTypeCourseUpdatePage.enterData();

    expect(await voucherTypeCourseComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(voucherTypeCourseComponentsPage.table);
    await waitUntilCount(voucherTypeCourseComponentsPage.records, beforeRecordsCount + 1);
    expect(await voucherTypeCourseComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await voucherTypeCourseComponentsPage.deleteVoucherTypeCourse();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(voucherTypeCourseComponentsPage.records, beforeRecordsCount);
      expect(await voucherTypeCourseComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(voucherTypeCourseComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
