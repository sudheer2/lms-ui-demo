import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class CourseUpdatePage {
  pageTitle: ElementFinder = element(by.id('trainsLmsUiApp.course.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  courseNameInput: ElementFinder = element(by.css('input#course-courseName'));
  courseTypeSelect: ElementFinder = element(by.css('select#course-courseType'));
  durationInput: ElementFinder = element(by.css('input#course-duration'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCourseNameInput(courseName) {
    await this.courseNameInput.sendKeys(courseName);
  }

  async getCourseNameInput() {
    return this.courseNameInput.getAttribute('value');
  }

  async setCourseTypeSelect(courseType) {
    await this.courseTypeSelect.sendKeys(courseType);
  }

  async getCourseTypeSelect() {
    return this.courseTypeSelect.element(by.css('option:checked')).getText();
  }

  async courseTypeSelectLastOption() {
    await this.courseTypeSelect.all(by.tagName('option')).last().click();
  }
  async setDurationInput(duration) {
    await this.durationInput.sendKeys(duration);
  }

  async getDurationInput() {
    return this.durationInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setCourseNameInput('courseName');
    expect(await this.getCourseNameInput()).to.match(/courseName/);
    await waitUntilDisplayed(this.saveButton);
    await this.courseTypeSelectLastOption();
    await waitUntilDisplayed(this.saveButton);
    await this.setDurationInput('duration');
    expect(await this.getDurationInput()).to.match(/duration/);
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
