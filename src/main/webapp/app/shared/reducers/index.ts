import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import voucherType, {
  VoucherTypeState
} from 'app/entities/voucher-type/voucher-type.reducer';
// prettier-ignore
import course, {
  CourseState
} from 'app/entities/course/course.reducer';
// prettier-ignore
import student, {
  StudentState
} from 'app/entities/student/student.reducer';
// prettier-ignore
import voucher, {
  VoucherState
} from 'app/entities/voucher/voucher.reducer';
// prettier-ignore
import program, {
  ProgramState
} from 'app/entities/program/program.reducer';
// prettier-ignore
import laptopInventory, {
  LaptopInventoryState
} from 'app/entities/laptop-inventory/laptop-inventory.reducer';
// prettier-ignore
import laptopPickupSchedule, {
  LaptopPickupScheduleState
} from 'app/entities/laptop-pickup-schedule/laptop-pickup-schedule.reducer';
// prettier-ignore
import programCourses, {
  ProgramCoursesState
} from 'app/entities/program-courses/program-courses.reducer';
// prettier-ignore
import voucherTypeCourse, {
  VoucherTypeCourseState
} from 'app/entities/voucher-type-course/voucher-type-course.reducer';
// prettier-ignore
import laptopStudent, {
  LaptopStudentState
} from 'app/entities/laptop-student/laptop-student.reducer';
// prettier-ignore
import voucherStudent, {
  VoucherStudentState
} from 'app/entities/voucher-student/voucher-student.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly voucherType: VoucherTypeState;
  readonly course: CourseState;
  readonly student: StudentState;
  readonly voucher: VoucherState;
  readonly program: ProgramState;
  readonly laptopInventory: LaptopInventoryState;
  readonly laptopPickupSchedule: LaptopPickupScheduleState;
  readonly programCourses: ProgramCoursesState;
  readonly voucherTypeCourse: VoucherTypeCourseState;
  readonly laptopStudent: LaptopStudentState;
  readonly voucherStudent: VoucherStudentState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  voucherType,
  course,
  student,
  voucher,
  program,
  laptopInventory,
  laptopPickupSchedule,
  programCourses,
  voucherTypeCourse,
  laptopStudent,
  voucherStudent,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
