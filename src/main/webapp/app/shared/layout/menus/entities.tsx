import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown icon="th-list" name="Entities" id="entity-menu" style={{ maxHeight: '80vh', overflow: 'auto' }}>
    <MenuItem icon="asterisk" to="/voucher-type">
      Voucher Type
    </MenuItem>
    <MenuItem icon="asterisk" to="/course">
      Course
    </MenuItem>
    <MenuItem icon="asterisk" to="/student">
      Student
    </MenuItem>
    <MenuItem icon="asterisk" to="/voucher">
      Voucher
    </MenuItem>
    <MenuItem icon="asterisk" to="/program">
      Program
    </MenuItem>
    <MenuItem icon="asterisk" to="/laptop-inventory">
      Laptop Inventory
    </MenuItem>
    <MenuItem icon="asterisk" to="/laptop-pickup-schedule">
      Laptop Pickup Schedule
    </MenuItem>
    <MenuItem icon="asterisk" to="/program-courses">
      Program Courses
    </MenuItem>
    <MenuItem icon="asterisk" to="/voucher-type-course">
      Voucher Type Course
    </MenuItem>
    <MenuItem icon="asterisk" to="/laptop-student">
      Laptop Student
    </MenuItem>
    <MenuItem icon="asterisk" to="/voucher-student">
      Voucher Student
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
