export interface IVoucher {
  id?: number;
  voucherCode?: string;
  voucherName?: string;
  assigned?: boolean;
}

export const defaultValue: Readonly<IVoucher> = {
  assigned: false,
};
