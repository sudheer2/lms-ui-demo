import { IVoucher } from 'app/shared/model/voucher.model';
import { VoucherCategories } from 'app/shared/model/enumerations/voucher-categories.model';

export interface IVoucherType {
  id?: number;
  voucherCategory?: VoucherCategories;
  voucher?: IVoucher;
}

export const defaultValue: Readonly<IVoucherType> = {};
