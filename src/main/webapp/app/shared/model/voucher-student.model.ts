import { IVoucher } from 'app/shared/model/voucher.model';
import { ILaptopStudent } from 'app/shared/model/laptop-student.model';

export interface IVoucherStudent {
  id?: number;
  voucher?: IVoucher;
  laptopStudent?: ILaptopStudent;
}

export const defaultValue: Readonly<IVoucherStudent> = {};
