import { ICourse } from 'app/shared/model/course.model';
import { IProgram } from 'app/shared/model/program.model';
import { StudentStatus } from 'app/shared/model/enumerations/student-status.model';

export interface IStudent {
  id?: number;
  firstName?: string;
  lastName?: string;
  city?: string;
  state?: string;
  county?: string;
  zipcode?: string;
  emailId?: string;
  phoneNumber?: string;
  status?: StudentStatus;
  notes?: string;
  leadSource?: string;
  course?: ICourse;
  program?: IProgram;
}

export const defaultValue: Readonly<IStudent> = {};
