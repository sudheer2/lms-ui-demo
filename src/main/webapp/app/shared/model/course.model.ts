import { CourseType } from 'app/shared/model/enumerations/course-type.model';

export interface ICourse {
  id?: number;
  courseName?: string;
  courseType?: CourseType;
  duration?: string;
}

export const defaultValue: Readonly<ICourse> = {};
