import { ICourse } from 'app/shared/model/course.model';
import { IVoucher } from 'app/shared/model/voucher.model';

export interface IVoucherTypeCourse {
  id?: number;
  course?: ICourse;
  voucher?: IVoucher;
}

export const defaultValue: Readonly<IVoucherTypeCourse> = {};
