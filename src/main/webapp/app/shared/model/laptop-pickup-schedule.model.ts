import { Moment } from 'moment';
import { IProgram } from 'app/shared/model/program.model';

export interface ILaptopPickupSchedule {
  id?: number;
  date?: string;
  startHours?: string;
  endHours?: string;
  program?: IProgram;
}

export const defaultValue: Readonly<ILaptopPickupSchedule> = {};
