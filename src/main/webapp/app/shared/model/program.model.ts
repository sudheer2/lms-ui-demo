import { Moment } from 'moment';

export interface IProgram {
  id?: number;
  programName?: string;
  startDate?: string;
  endDate?: string;
  totalSeats?: number;
}

export const defaultValue: Readonly<IProgram> = {};
