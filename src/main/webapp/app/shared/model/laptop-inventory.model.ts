import { LaptopType } from 'app/shared/model/enumerations/laptop-type.model';

export interface ILaptopInventory {
  id?: number;
  name?: string;
  make?: LaptopType;
  model?: string;
  remoteConnectionId?: string;
  assigned?: boolean;
}

export const defaultValue: Readonly<ILaptopInventory> = {
  assigned: false,
};
