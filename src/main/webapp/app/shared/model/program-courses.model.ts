import { IProgram } from 'app/shared/model/program.model';
import { ICourse } from 'app/shared/model/course.model';

export interface IProgramCourses {
  id?: number;
  program?: IProgram;
  course?: ICourse;
}

export const defaultValue: Readonly<IProgramCourses> = {};
