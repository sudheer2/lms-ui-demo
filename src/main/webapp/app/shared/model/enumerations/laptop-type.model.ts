export const enum LaptopType {
  HP = 'HP',

  DELL = 'DELL',

  LENOVO = 'LENOVO',
}
