export const enum VoucherCategories {
  DIGITAL_CHALK = 'DIGITAL_CHALK',

  EBOOK1 = 'EBOOK1',

  EBOOK2 = 'EBOOK2',

  LAB = 'LAB',

  EXAM_PREP = 'EXAM_PREP',

  EXAM = 'EXAM',
}
