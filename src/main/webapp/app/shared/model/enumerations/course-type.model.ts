export const enum CourseType {
  IN_PERSON = 'IN_PERSON',

  VIRTUAL = 'VIRTUAL',

  SELF_PACED = 'SELF_PACED',
}
