import { IStudent } from 'app/shared/model/student.model';
import { ILaptopInventory } from 'app/shared/model/laptop-inventory.model';
import { IVoucherStudent } from 'app/shared/model/voucher-student.model';

export interface ILaptopStudent {
  id?: number;
  student?: IStudent;
  laptop?: ILaptopInventory;
  laptopAssignments?: IVoucherStudent[];
}

export const defaultValue: Readonly<ILaptopStudent> = {};
