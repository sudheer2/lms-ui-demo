import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICourse } from 'app/shared/model/course.model';
import { getEntities as getCourses } from 'app/entities/course/course.reducer';
import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './student.reducer';
import { IStudent } from 'app/shared/model/student.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IStudentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StudentUpdate = (props: IStudentUpdateProps) => {
  const [courseId, setCourseId] = useState('0');
  const [programId, setProgramId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { studentEntity, courses, programs, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/student');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getCourses();
    props.getPrograms();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...studentEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.student.home.createOrEditLabel">Create or edit a Student</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : studentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="student-id">ID</Label>
                  <AvInput id="student-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="firstNameLabel" for="student-firstName">
                  First Name
                </Label>
                <AvField
                  id="student-firstName"
                  type="text"
                  name="firstName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="lastNameLabel" for="student-lastName">
                  Last Name
                </Label>
                <AvField
                  id="student-lastName"
                  type="text"
                  name="lastName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="cityLabel" for="student-city">
                  City
                </Label>
                <AvField id="student-city" type="text" name="city" />
              </AvGroup>
              <AvGroup>
                <Label id="stateLabel" for="student-state">
                  State
                </Label>
                <AvField id="student-state" type="text" name="state" />
              </AvGroup>
              <AvGroup>
                <Label id="countyLabel" for="student-county">
                  County
                </Label>
                <AvField id="student-county" type="text" name="county" />
              </AvGroup>
              <AvGroup>
                <Label id="zipcodeLabel" for="student-zipcode">
                  Zipcode
                </Label>
                <AvField id="student-zipcode" type="text" name="zipcode" />
              </AvGroup>
              <AvGroup>
                <Label id="emailIdLabel" for="student-emailId">
                  Email Id
                </Label>
                <AvField
                  id="student-emailId"
                  type="text"
                  name="emailId"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="phoneNumberLabel" for="student-phoneNumber">
                  Phone Number
                </Label>
                <AvField
                  id="student-phoneNumber"
                  type="text"
                  name="phoneNumber"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="statusLabel" for="student-status">
                  Status
                </Label>
                <AvInput
                  id="student-status"
                  type="select"
                  className="form-control"
                  name="status"
                  value={(!isNew && studentEntity.status) || 'SIGNED_UP'}
                >
                  <option value="SIGNED_UP">SIGNED_UP</option>
                  <option value="WAITING_LIST">WAITING_LIST</option>
                  <option value="QUALIFIED">QUALIFIED</option>
                  <option value="DROPPED">DROPPED</option>
                  <option value="SCHEDULED">SCHEDULED</option>
                  <option value="TRAINING">TRAINING</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="notesLabel" for="student-notes">
                  Notes
                </Label>
                <AvField id="student-notes" type="text" name="notes" />
              </AvGroup>
              <AvGroup>
                <Label id="leadSourceLabel" for="student-leadSource">
                  Lead Source
                </Label>
                <AvField id="student-leadSource" type="text" name="leadSource" />
              </AvGroup>
              <AvGroup>
                <Label for="student-course">Course</Label>
                <AvInput id="student-course" type="select" className="form-control" name="course.id">
                  <option value="" key="0" />
                  {courses
                    ? courses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="student-program">Program</Label>
                <AvInput id="student-program" type="select" className="form-control" name="program.id">
                  <option value="" key="0" />
                  {programs
                    ? programs.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/student" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  courses: storeState.course.entities,
  programs: storeState.program.entities,
  studentEntity: storeState.student.entity,
  loading: storeState.student.loading,
  updating: storeState.student.updating,
  updateSuccess: storeState.student.updateSuccess,
});

const mapDispatchToProps = {
  getCourses,
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StudentUpdate);
