import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './student.reducer';
import { IStudent } from 'app/shared/model/student.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IStudentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const StudentDetail = (props: IStudentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { studentEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Student [<b>{studentEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="firstName">First Name</span>
          </dt>
          <dd>{studentEntity.firstName}</dd>
          <dt>
            <span id="lastName">Last Name</span>
          </dt>
          <dd>{studentEntity.lastName}</dd>
          <dt>
            <span id="city">City</span>
          </dt>
          <dd>{studentEntity.city}</dd>
          <dt>
            <span id="state">State</span>
          </dt>
          <dd>{studentEntity.state}</dd>
          <dt>
            <span id="county">County</span>
          </dt>
          <dd>{studentEntity.county}</dd>
          <dt>
            <span id="zipcode">Zipcode</span>
          </dt>
          <dd>{studentEntity.zipcode}</dd>
          <dt>
            <span id="emailId">Email Id</span>
          </dt>
          <dd>{studentEntity.emailId}</dd>
          <dt>
            <span id="phoneNumber">Phone Number</span>
          </dt>
          <dd>{studentEntity.phoneNumber}</dd>
          <dt>
            <span id="status">Status</span>
          </dt>
          <dd>{studentEntity.status}</dd>
          <dt>
            <span id="notes">Notes</span>
          </dt>
          <dd>{studentEntity.notes}</dd>
          <dt>
            <span id="leadSource">Lead Source</span>
          </dt>
          <dd>{studentEntity.leadSource}</dd>
          <dt>Course</dt>
          <dd>{studentEntity.course ? studentEntity.course.id : ''}</dd>
          <dt>Program</dt>
          <dd>{studentEntity.program ? studentEntity.program.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/student" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/student/${studentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ student }: IRootState) => ({
  studentEntity: student.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(StudentDetail);
