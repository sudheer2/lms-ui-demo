import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program.reducer';
import { IProgram } from 'app/shared/model/program.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Program = (props: IProgramProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { programList, match, loading } = props;
  return (
    <div>
      <h2 id="program-heading">
        Programs
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Program
        </Link>
      </h2>
      <div className="table-responsive">
        {programList && programList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Program Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Total Seats</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programList.map((program, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${program.id}`} color="link" size="sm">
                      {program.id}
                    </Button>
                  </td>
                  <td>{program.programName}</td>
                  <td>{program.startDate ? <TextFormat type="date" value={program.startDate} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td>{program.endDate ? <TextFormat type="date" value={program.endDate} format={APP_LOCAL_DATE_FORMAT} /> : null}</td>
                  <td>{program.totalSeats}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${program.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${program.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${program.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Programs found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ program }: IRootState) => ({
  programList: program.entities,
  loading: program.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Program);
