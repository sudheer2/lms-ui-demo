import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program.reducer';
import { IProgram } from 'app/shared/model/program.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProgramDetail = (props: IProgramDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { programEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Program [<b>{programEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="programName">Program Name</span>
          </dt>
          <dd>{programEntity.programName}</dd>
          <dt>
            <span id="startDate">Start Date</span>
          </dt>
          <dd>
            {programEntity.startDate ? <TextFormat value={programEntity.startDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="endDate">End Date</span>
          </dt>
          <dd>{programEntity.endDate ? <TextFormat value={programEntity.endDate} type="date" format={APP_LOCAL_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="totalSeats">Total Seats</span>
          </dt>
          <dd>{programEntity.totalSeats}</dd>
        </dl>
        <Button tag={Link} to="/program" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/program/${programEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ program }: IRootState) => ({
  programEntity: program.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProgramDetail);
