import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import VoucherType from './voucher-type';
import Course from './course';
import Student from './student';
import Voucher from './voucher';
import Program from './program';
import LaptopInventory from './laptop-inventory';
import LaptopPickupSchedule from './laptop-pickup-schedule';
import ProgramCourses from './program-courses';
import VoucherTypeCourse from './voucher-type-course';
import LaptopStudent from './laptop-student';
import VoucherStudent from './voucher-student';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}voucher-type`} component={VoucherType} />
      <ErrorBoundaryRoute path={`${match.url}course`} component={Course} />
      <ErrorBoundaryRoute path={`${match.url}student`} component={Student} />
      <ErrorBoundaryRoute path={`${match.url}voucher`} component={Voucher} />
      <ErrorBoundaryRoute path={`${match.url}program`} component={Program} />
      <ErrorBoundaryRoute path={`${match.url}laptop-inventory`} component={LaptopInventory} />
      <ErrorBoundaryRoute path={`${match.url}laptop-pickup-schedule`} component={LaptopPickupSchedule} />
      <ErrorBoundaryRoute path={`${match.url}program-courses`} component={ProgramCourses} />
      <ErrorBoundaryRoute path={`${match.url}voucher-type-course`} component={VoucherTypeCourse} />
      <ErrorBoundaryRoute path={`${match.url}laptop-student`} component={LaptopStudent} />
      <ErrorBoundaryRoute path={`${match.url}voucher-student`} component={VoucherStudent} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
