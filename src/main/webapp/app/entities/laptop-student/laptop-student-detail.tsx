import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './laptop-student.reducer';
import { ILaptopStudent } from 'app/shared/model/laptop-student.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopStudentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopStudentDetail = (props: ILaptopStudentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { laptopStudentEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          LaptopStudent [<b>{laptopStudentEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>Student</dt>
          <dd>{laptopStudentEntity.student ? laptopStudentEntity.student.id : ''}</dd>
          <dt>Laptop</dt>
          <dd>{laptopStudentEntity.laptop ? laptopStudentEntity.laptop.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/laptop-student" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/laptop-student/${laptopStudentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ laptopStudent }: IRootState) => ({
  laptopStudentEntity: laptopStudent.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopStudentDetail);
