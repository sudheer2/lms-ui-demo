import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './laptop-student.reducer';
import { ILaptopStudent } from 'app/shared/model/laptop-student.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopStudentProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const LaptopStudent = (props: ILaptopStudentProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { laptopStudentList, match, loading } = props;
  return (
    <div>
      <h2 id="laptop-student-heading">
        Laptop Students
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Laptop Student
        </Link>
      </h2>
      <div className="table-responsive">
        {laptopStudentList && laptopStudentList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Student</th>
                <th>Laptop</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {laptopStudentList.map((laptopStudent, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${laptopStudent.id}`} color="link" size="sm">
                      {laptopStudent.id}
                    </Button>
                  </td>
                  <td>{laptopStudent.student ? <Link to={`student/${laptopStudent.student.id}`}>{laptopStudent.student.id}</Link> : ''}</td>
                  <td>
                    {laptopStudent.laptop ? <Link to={`laptop-inventory/${laptopStudent.laptop.id}`}>{laptopStudent.laptop.id}</Link> : ''}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${laptopStudent.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopStudent.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopStudent.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Laptop Students found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ laptopStudent }: IRootState) => ({
  laptopStudentList: laptopStudent.entities,
  loading: laptopStudent.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopStudent);
