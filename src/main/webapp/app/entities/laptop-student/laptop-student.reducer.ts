import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILaptopStudent, defaultValue } from 'app/shared/model/laptop-student.model';

export const ACTION_TYPES = {
  FETCH_LAPTOPSTUDENT_LIST: 'laptopStudent/FETCH_LAPTOPSTUDENT_LIST',
  FETCH_LAPTOPSTUDENT: 'laptopStudent/FETCH_LAPTOPSTUDENT',
  CREATE_LAPTOPSTUDENT: 'laptopStudent/CREATE_LAPTOPSTUDENT',
  UPDATE_LAPTOPSTUDENT: 'laptopStudent/UPDATE_LAPTOPSTUDENT',
  DELETE_LAPTOPSTUDENT: 'laptopStudent/DELETE_LAPTOPSTUDENT',
  RESET: 'laptopStudent/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILaptopStudent>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LaptopStudentState = Readonly<typeof initialState>;

// Reducer

export default (state: LaptopStudentState = initialState, action): LaptopStudentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPSTUDENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPSTUDENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LAPTOPSTUDENT):
    case REQUEST(ACTION_TYPES.UPDATE_LAPTOPSTUDENT):
    case REQUEST(ACTION_TYPES.DELETE_LAPTOPSTUDENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPSTUDENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPSTUDENT):
    case FAILURE(ACTION_TYPES.CREATE_LAPTOPSTUDENT):
    case FAILURE(ACTION_TYPES.UPDATE_LAPTOPSTUDENT):
    case FAILURE(ACTION_TYPES.DELETE_LAPTOPSTUDENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPSTUDENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPSTUDENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LAPTOPSTUDENT):
    case SUCCESS(ACTION_TYPES.UPDATE_LAPTOPSTUDENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LAPTOPSTUDENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/laptop-students';

// Actions

export const getEntities: ICrudGetAllAction<ILaptopStudent> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LAPTOPSTUDENT_LIST,
  payload: axios.get<ILaptopStudent>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILaptopStudent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LAPTOPSTUDENT,
    payload: axios.get<ILaptopStudent>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILaptopStudent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LAPTOPSTUDENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILaptopStudent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LAPTOPSTUDENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILaptopStudent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LAPTOPSTUDENT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
