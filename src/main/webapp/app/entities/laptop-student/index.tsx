import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LaptopStudent from './laptop-student';
import LaptopStudentDetail from './laptop-student-detail';
import LaptopStudentUpdate from './laptop-student-update';
import LaptopStudentDeleteDialog from './laptop-student-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LaptopStudentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LaptopStudentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LaptopStudentDetail} />
      <ErrorBoundaryRoute path={match.url} component={LaptopStudent} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LaptopStudentDeleteDialog} />
  </>
);

export default Routes;
