import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IStudent } from 'app/shared/model/student.model';
import { getEntities as getStudents } from 'app/entities/student/student.reducer';
import { ILaptopInventory } from 'app/shared/model/laptop-inventory.model';
import { getEntities as getLaptopInventories } from 'app/entities/laptop-inventory/laptop-inventory.reducer';
import { getEntity, updateEntity, createEntity, reset } from './laptop-student.reducer';
import { ILaptopStudent } from 'app/shared/model/laptop-student.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILaptopStudentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopStudentUpdate = (props: ILaptopStudentUpdateProps) => {
  const [studentId, setStudentId] = useState('0');
  const [laptopId, setLaptopId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { laptopStudentEntity, students, laptopInventories, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/laptop-student');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getStudents();
    props.getLaptopInventories();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...laptopStudentEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.laptopStudent.home.createOrEditLabel">Create or edit a LaptopStudent</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : laptopStudentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="laptop-student-id">ID</Label>
                  <AvInput id="laptop-student-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="laptop-student-student">Student</Label>
                <AvInput id="laptop-student-student" type="select" className="form-control" name="student.id">
                  <option value="" key="0" />
                  {students
                    ? students.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="laptop-student-laptop">Laptop</Label>
                <AvInput id="laptop-student-laptop" type="select" className="form-control" name="laptop.id">
                  <option value="" key="0" />
                  {laptopInventories
                    ? laptopInventories.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/laptop-student" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  students: storeState.student.entities,
  laptopInventories: storeState.laptopInventory.entities,
  laptopStudentEntity: storeState.laptopStudent.entity,
  loading: storeState.laptopStudent.loading,
  updating: storeState.laptopStudent.updating,
  updateSuccess: storeState.laptopStudent.updateSuccess,
});

const mapDispatchToProps = {
  getStudents,
  getLaptopInventories,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopStudentUpdate);
