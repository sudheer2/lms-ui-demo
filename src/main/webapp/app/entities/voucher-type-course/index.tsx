import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import VoucherTypeCourse from './voucher-type-course';
import VoucherTypeCourseDetail from './voucher-type-course-detail';
import VoucherTypeCourseUpdate from './voucher-type-course-update';
import VoucherTypeCourseDeleteDialog from './voucher-type-course-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={VoucherTypeCourseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={VoucherTypeCourseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={VoucherTypeCourseDetail} />
      <ErrorBoundaryRoute path={match.url} component={VoucherTypeCourse} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={VoucherTypeCourseDeleteDialog} />
  </>
);

export default Routes;
