import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './voucher-type-course.reducer';
import { IVoucherTypeCourse } from 'app/shared/model/voucher-type-course.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVoucherTypeCourseProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const VoucherTypeCourse = (props: IVoucherTypeCourseProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { voucherTypeCourseList, match, loading } = props;
  return (
    <div>
      <h2 id="voucher-type-course-heading">
        Voucher Type Courses
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Voucher Type Course
        </Link>
      </h2>
      <div className="table-responsive">
        {voucherTypeCourseList && voucherTypeCourseList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Course</th>
                <th>Voucher</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {voucherTypeCourseList.map((voucherTypeCourse, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${voucherTypeCourse.id}`} color="link" size="sm">
                      {voucherTypeCourse.id}
                    </Button>
                  </td>
                  <td>
                    {voucherTypeCourse.course ? (
                      <Link to={`course/${voucherTypeCourse.course.id}`}>{voucherTypeCourse.course.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>
                    {voucherTypeCourse.voucher ? (
                      <Link to={`voucher/${voucherTypeCourse.voucher.id}`}>{voucherTypeCourse.voucher.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${voucherTypeCourse.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${voucherTypeCourse.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${voucherTypeCourse.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Voucher Type Courses found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ voucherTypeCourse }: IRootState) => ({
  voucherTypeCourseList: voucherTypeCourse.entities,
  loading: voucherTypeCourse.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherTypeCourse);
