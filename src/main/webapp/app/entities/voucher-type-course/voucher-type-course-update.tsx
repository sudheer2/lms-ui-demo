import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ICourse } from 'app/shared/model/course.model';
import { getEntities as getCourses } from 'app/entities/course/course.reducer';
import { IVoucher } from 'app/shared/model/voucher.model';
import { getEntities as getVouchers } from 'app/entities/voucher/voucher.reducer';
import { getEntity, updateEntity, createEntity, reset } from './voucher-type-course.reducer';
import { IVoucherTypeCourse } from 'app/shared/model/voucher-type-course.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IVoucherTypeCourseUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherTypeCourseUpdate = (props: IVoucherTypeCourseUpdateProps) => {
  const [courseId, setCourseId] = useState('0');
  const [voucherId, setVoucherId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { voucherTypeCourseEntity, courses, vouchers, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/voucher-type-course');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getCourses();
    props.getVouchers();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...voucherTypeCourseEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.voucherTypeCourse.home.createOrEditLabel">Create or edit a VoucherTypeCourse</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : voucherTypeCourseEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="voucher-type-course-id">ID</Label>
                  <AvInput id="voucher-type-course-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="voucher-type-course-course">Course</Label>
                <AvInput id="voucher-type-course-course" type="select" className="form-control" name="course.id">
                  <option value="" key="0" />
                  {courses
                    ? courses.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="voucher-type-course-voucher">Voucher</Label>
                <AvInput id="voucher-type-course-voucher" type="select" className="form-control" name="voucher.id">
                  <option value="" key="0" />
                  {vouchers
                    ? vouchers.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/voucher-type-course" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  courses: storeState.course.entities,
  vouchers: storeState.voucher.entities,
  voucherTypeCourseEntity: storeState.voucherTypeCourse.entity,
  loading: storeState.voucherTypeCourse.loading,
  updating: storeState.voucherTypeCourse.updating,
  updateSuccess: storeState.voucherTypeCourse.updateSuccess,
});

const mapDispatchToProps = {
  getCourses,
  getVouchers,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherTypeCourseUpdate);
