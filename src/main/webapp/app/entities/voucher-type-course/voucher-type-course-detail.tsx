import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './voucher-type-course.reducer';
import { IVoucherTypeCourse } from 'app/shared/model/voucher-type-course.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVoucherTypeCourseDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherTypeCourseDetail = (props: IVoucherTypeCourseDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { voucherTypeCourseEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          VoucherTypeCourse [<b>{voucherTypeCourseEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>Course</dt>
          <dd>{voucherTypeCourseEntity.course ? voucherTypeCourseEntity.course.id : ''}</dd>
          <dt>Voucher</dt>
          <dd>{voucherTypeCourseEntity.voucher ? voucherTypeCourseEntity.voucher.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/voucher-type-course" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/voucher-type-course/${voucherTypeCourseEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ voucherTypeCourse }: IRootState) => ({
  voucherTypeCourseEntity: voucherTypeCourse.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherTypeCourseDetail);
