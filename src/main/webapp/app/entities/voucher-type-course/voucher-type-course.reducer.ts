import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IVoucherTypeCourse, defaultValue } from 'app/shared/model/voucher-type-course.model';

export const ACTION_TYPES = {
  FETCH_VOUCHERTYPECOURSE_LIST: 'voucherTypeCourse/FETCH_VOUCHERTYPECOURSE_LIST',
  FETCH_VOUCHERTYPECOURSE: 'voucherTypeCourse/FETCH_VOUCHERTYPECOURSE',
  CREATE_VOUCHERTYPECOURSE: 'voucherTypeCourse/CREATE_VOUCHERTYPECOURSE',
  UPDATE_VOUCHERTYPECOURSE: 'voucherTypeCourse/UPDATE_VOUCHERTYPECOURSE',
  DELETE_VOUCHERTYPECOURSE: 'voucherTypeCourse/DELETE_VOUCHERTYPECOURSE',
  RESET: 'voucherTypeCourse/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IVoucherTypeCourse>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type VoucherTypeCourseState = Readonly<typeof initialState>;

// Reducer

export default (state: VoucherTypeCourseState = initialState, action): VoucherTypeCourseState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_VOUCHERTYPECOURSE):
    case REQUEST(ACTION_TYPES.UPDATE_VOUCHERTYPECOURSE):
    case REQUEST(ACTION_TYPES.DELETE_VOUCHERTYPECOURSE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE):
    case FAILURE(ACTION_TYPES.CREATE_VOUCHERTYPECOURSE):
    case FAILURE(ACTION_TYPES.UPDATE_VOUCHERTYPECOURSE):
    case FAILURE(ACTION_TYPES.DELETE_VOUCHERTYPECOURSE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERTYPECOURSE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_VOUCHERTYPECOURSE):
    case SUCCESS(ACTION_TYPES.UPDATE_VOUCHERTYPECOURSE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_VOUCHERTYPECOURSE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/voucher-type-courses';

// Actions

export const getEntities: ICrudGetAllAction<IVoucherTypeCourse> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_VOUCHERTYPECOURSE_LIST,
  payload: axios.get<IVoucherTypeCourse>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IVoucherTypeCourse> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_VOUCHERTYPECOURSE,
    payload: axios.get<IVoucherTypeCourse>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IVoucherTypeCourse> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_VOUCHERTYPECOURSE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IVoucherTypeCourse> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_VOUCHERTYPECOURSE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IVoucherTypeCourse> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_VOUCHERTYPECOURSE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
