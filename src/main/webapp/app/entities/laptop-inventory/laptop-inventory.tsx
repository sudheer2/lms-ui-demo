import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './laptop-inventory.reducer';
import { ILaptopInventory } from 'app/shared/model/laptop-inventory.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopInventoryProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const LaptopInventory = (props: ILaptopInventoryProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { laptopInventoryList, match, loading } = props;
  return (
    <div>
      <h2 id="laptop-inventory-heading">
        Laptop Inventories
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Laptop Inventory
        </Link>
      </h2>
      <div className="table-responsive">
        {laptopInventoryList && laptopInventoryList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Make</th>
                <th>Model</th>
                <th>Remote Connection Id</th>
                <th>Assigned</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {laptopInventoryList.map((laptopInventory, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${laptopInventory.id}`} color="link" size="sm">
                      {laptopInventory.id}
                    </Button>
                  </td>
                  <td>{laptopInventory.name}</td>
                  <td>{laptopInventory.make}</td>
                  <td>{laptopInventory.model}</td>
                  <td>{laptopInventory.remoteConnectionId}</td>
                  <td>{laptopInventory.assigned ? 'true' : 'false'}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${laptopInventory.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopInventory.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopInventory.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Laptop Inventories found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ laptopInventory }: IRootState) => ({
  laptopInventoryList: laptopInventory.entities,
  loading: laptopInventory.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopInventory);
