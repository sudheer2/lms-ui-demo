import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LaptopInventory from './laptop-inventory';
import LaptopInventoryDetail from './laptop-inventory-detail';
import LaptopInventoryUpdate from './laptop-inventory-update';
import LaptopInventoryDeleteDialog from './laptop-inventory-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LaptopInventoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LaptopInventoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LaptopInventoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={LaptopInventory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LaptopInventoryDeleteDialog} />
  </>
);

export default Routes;
