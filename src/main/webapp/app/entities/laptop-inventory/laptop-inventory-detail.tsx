import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './laptop-inventory.reducer';
import { ILaptopInventory } from 'app/shared/model/laptop-inventory.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopInventoryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopInventoryDetail = (props: ILaptopInventoryDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { laptopInventoryEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          LaptopInventory [<b>{laptopInventoryEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">Name</span>
          </dt>
          <dd>{laptopInventoryEntity.name}</dd>
          <dt>
            <span id="make">Make</span>
          </dt>
          <dd>{laptopInventoryEntity.make}</dd>
          <dt>
            <span id="model">Model</span>
          </dt>
          <dd>{laptopInventoryEntity.model}</dd>
          <dt>
            <span id="remoteConnectionId">Remote Connection Id</span>
          </dt>
          <dd>{laptopInventoryEntity.remoteConnectionId}</dd>
          <dt>
            <span id="assigned">Assigned</span>
          </dt>
          <dd>{laptopInventoryEntity.assigned ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/laptop-inventory" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/laptop-inventory/${laptopInventoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ laptopInventory }: IRootState) => ({
  laptopInventoryEntity: laptopInventory.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopInventoryDetail);
