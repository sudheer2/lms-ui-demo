import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILaptopInventory, defaultValue } from 'app/shared/model/laptop-inventory.model';

export const ACTION_TYPES = {
  FETCH_LAPTOPINVENTORY_LIST: 'laptopInventory/FETCH_LAPTOPINVENTORY_LIST',
  FETCH_LAPTOPINVENTORY: 'laptopInventory/FETCH_LAPTOPINVENTORY',
  CREATE_LAPTOPINVENTORY: 'laptopInventory/CREATE_LAPTOPINVENTORY',
  UPDATE_LAPTOPINVENTORY: 'laptopInventory/UPDATE_LAPTOPINVENTORY',
  DELETE_LAPTOPINVENTORY: 'laptopInventory/DELETE_LAPTOPINVENTORY',
  RESET: 'laptopInventory/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILaptopInventory>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LaptopInventoryState = Readonly<typeof initialState>;

// Reducer

export default (state: LaptopInventoryState = initialState, action): LaptopInventoryState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPINVENTORY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPINVENTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LAPTOPINVENTORY):
    case REQUEST(ACTION_TYPES.UPDATE_LAPTOPINVENTORY):
    case REQUEST(ACTION_TYPES.DELETE_LAPTOPINVENTORY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPINVENTORY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPINVENTORY):
    case FAILURE(ACTION_TYPES.CREATE_LAPTOPINVENTORY):
    case FAILURE(ACTION_TYPES.UPDATE_LAPTOPINVENTORY):
    case FAILURE(ACTION_TYPES.DELETE_LAPTOPINVENTORY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPINVENTORY_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPINVENTORY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LAPTOPINVENTORY):
    case SUCCESS(ACTION_TYPES.UPDATE_LAPTOPINVENTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LAPTOPINVENTORY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/laptop-inventories';

// Actions

export const getEntities: ICrudGetAllAction<ILaptopInventory> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LAPTOPINVENTORY_LIST,
  payload: axios.get<ILaptopInventory>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILaptopInventory> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LAPTOPINVENTORY,
    payload: axios.get<ILaptopInventory>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILaptopInventory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LAPTOPINVENTORY,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILaptopInventory> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LAPTOPINVENTORY,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILaptopInventory> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LAPTOPINVENTORY,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
