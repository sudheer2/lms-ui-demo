import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './laptop-inventory.reducer';
import { ILaptopInventory } from 'app/shared/model/laptop-inventory.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILaptopInventoryUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopInventoryUpdate = (props: ILaptopInventoryUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { laptopInventoryEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/laptop-inventory');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...laptopInventoryEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.laptopInventory.home.createOrEditLabel">Create or edit a LaptopInventory</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : laptopInventoryEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="laptop-inventory-id">ID</Label>
                  <AvInput id="laptop-inventory-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nameLabel" for="laptop-inventory-name">
                  Name
                </Label>
                <AvField
                  id="laptop-inventory-name"
                  type="text"
                  name="name"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="makeLabel" for="laptop-inventory-make">
                  Make
                </Label>
                <AvInput
                  id="laptop-inventory-make"
                  type="select"
                  className="form-control"
                  name="make"
                  value={(!isNew && laptopInventoryEntity.make) || 'HP'}
                >
                  <option value="HP">HP</option>
                  <option value="DELL">DELL</option>
                  <option value="LENOVO">LENOVO</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label id="modelLabel" for="laptop-inventory-model">
                  Model
                </Label>
                <AvField id="laptop-inventory-model" type="text" name="model" />
              </AvGroup>
              <AvGroup>
                <Label id="remoteConnectionIdLabel" for="laptop-inventory-remoteConnectionId">
                  Remote Connection Id
                </Label>
                <AvField
                  id="laptop-inventory-remoteConnectionId"
                  type="text"
                  name="remoteConnectionId"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="assignedLabel">
                  <AvInput id="laptop-inventory-assigned" type="checkbox" className="form-check-input" name="assigned" />
                  Assigned
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/laptop-inventory" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  laptopInventoryEntity: storeState.laptopInventory.entity,
  loading: storeState.laptopInventory.loading,
  updating: storeState.laptopInventory.updating,
  updateSuccess: storeState.laptopInventory.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopInventoryUpdate);
