import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { ILaptopPickupSchedule } from 'app/shared/model/laptop-pickup-schedule.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './laptop-pickup-schedule.reducer';

export interface ILaptopPickupScheduleDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopPickupScheduleDeleteDialog = (props: ILaptopPickupScheduleDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/laptop-pickup-schedule');
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.laptopPickupScheduleEntity.id);
  };

  const { laptopPickupScheduleEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose}>Confirm delete operation</ModalHeader>
      <ModalBody id="trainsLmsUiApp.laptopPickupSchedule.delete.question">
        Are you sure you want to delete this LaptopPickupSchedule?
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp; Cancel
        </Button>
        <Button id="jhi-confirm-delete-laptopPickupSchedule" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp; Delete
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ laptopPickupSchedule }: IRootState) => ({
  laptopPickupScheduleEntity: laptopPickupSchedule.entity,
  updateSuccess: laptopPickupSchedule.updateSuccess,
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopPickupScheduleDeleteDialog);
