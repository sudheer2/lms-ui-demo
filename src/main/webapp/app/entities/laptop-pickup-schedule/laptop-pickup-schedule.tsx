import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './laptop-pickup-schedule.reducer';
import { ILaptopPickupSchedule } from 'app/shared/model/laptop-pickup-schedule.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopPickupScheduleProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const LaptopPickupSchedule = (props: ILaptopPickupScheduleProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { laptopPickupScheduleList, match, loading } = props;
  return (
    <div>
      <h2 id="laptop-pickup-schedule-heading">
        Laptop Pickup Schedules
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Laptop Pickup Schedule
        </Link>
      </h2>
      <div className="table-responsive">
        {laptopPickupScheduleList && laptopPickupScheduleList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Start Hours</th>
                <th>End Hours</th>
                <th>Program</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {laptopPickupScheduleList.map((laptopPickupSchedule, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${laptopPickupSchedule.id}`} color="link" size="sm">
                      {laptopPickupSchedule.id}
                    </Button>
                  </td>
                  <td>
                    {laptopPickupSchedule.date ? (
                      <TextFormat type="date" value={laptopPickupSchedule.date} format={APP_LOCAL_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{laptopPickupSchedule.startHours}</td>
                  <td>{laptopPickupSchedule.endHours}</td>
                  <td>
                    {laptopPickupSchedule.program ? (
                      <Link to={`program/${laptopPickupSchedule.program.id}`}>{laptopPickupSchedule.program.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${laptopPickupSchedule.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopPickupSchedule.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${laptopPickupSchedule.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Laptop Pickup Schedules found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ laptopPickupSchedule }: IRootState) => ({
  laptopPickupScheduleList: laptopPickupSchedule.entities,
  loading: laptopPickupSchedule.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopPickupSchedule);
