import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILaptopPickupSchedule, defaultValue } from 'app/shared/model/laptop-pickup-schedule.model';

export const ACTION_TYPES = {
  FETCH_LAPTOPPICKUPSCHEDULE_LIST: 'laptopPickupSchedule/FETCH_LAPTOPPICKUPSCHEDULE_LIST',
  FETCH_LAPTOPPICKUPSCHEDULE: 'laptopPickupSchedule/FETCH_LAPTOPPICKUPSCHEDULE',
  CREATE_LAPTOPPICKUPSCHEDULE: 'laptopPickupSchedule/CREATE_LAPTOPPICKUPSCHEDULE',
  UPDATE_LAPTOPPICKUPSCHEDULE: 'laptopPickupSchedule/UPDATE_LAPTOPPICKUPSCHEDULE',
  DELETE_LAPTOPPICKUPSCHEDULE: 'laptopPickupSchedule/DELETE_LAPTOPPICKUPSCHEDULE',
  RESET: 'laptopPickupSchedule/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILaptopPickupSchedule>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LaptopPickupScheduleState = Readonly<typeof initialState>;

// Reducer

export default (state: LaptopPickupScheduleState = initialState, action): LaptopPickupScheduleState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LAPTOPPICKUPSCHEDULE):
    case REQUEST(ACTION_TYPES.UPDATE_LAPTOPPICKUPSCHEDULE):
    case REQUEST(ACTION_TYPES.DELETE_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE):
    case FAILURE(ACTION_TYPES.CREATE_LAPTOPPICKUPSCHEDULE):
    case FAILURE(ACTION_TYPES.UPDATE_LAPTOPPICKUPSCHEDULE):
    case FAILURE(ACTION_TYPES.DELETE_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LAPTOPPICKUPSCHEDULE):
    case SUCCESS(ACTION_TYPES.UPDATE_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LAPTOPPICKUPSCHEDULE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/laptop-pickup-schedules';

// Actions

export const getEntities: ICrudGetAllAction<ILaptopPickupSchedule> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE_LIST,
  payload: axios.get<ILaptopPickupSchedule>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILaptopPickupSchedule> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LAPTOPPICKUPSCHEDULE,
    payload: axios.get<ILaptopPickupSchedule>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILaptopPickupSchedule> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LAPTOPPICKUPSCHEDULE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILaptopPickupSchedule> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LAPTOPPICKUPSCHEDULE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILaptopPickupSchedule> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LAPTOPPICKUPSCHEDULE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
