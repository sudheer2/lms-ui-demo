import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './laptop-pickup-schedule.reducer';
import { ILaptopPickupSchedule } from 'app/shared/model/laptop-pickup-schedule.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILaptopPickupScheduleDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LaptopPickupScheduleDetail = (props: ILaptopPickupScheduleDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { laptopPickupScheduleEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          LaptopPickupSchedule [<b>{laptopPickupScheduleEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="date">Date</span>
          </dt>
          <dd>
            {laptopPickupScheduleEntity.date ? (
              <TextFormat value={laptopPickupScheduleEntity.date} type="date" format={APP_LOCAL_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="startHours">Start Hours</span>
          </dt>
          <dd>{laptopPickupScheduleEntity.startHours}</dd>
          <dt>
            <span id="endHours">End Hours</span>
          </dt>
          <dd>{laptopPickupScheduleEntity.endHours}</dd>
          <dt>Program</dt>
          <dd>{laptopPickupScheduleEntity.program ? laptopPickupScheduleEntity.program.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/laptop-pickup-schedule" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/laptop-pickup-schedule/${laptopPickupScheduleEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ laptopPickupSchedule }: IRootState) => ({
  laptopPickupScheduleEntity: laptopPickupSchedule.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LaptopPickupScheduleDetail);
