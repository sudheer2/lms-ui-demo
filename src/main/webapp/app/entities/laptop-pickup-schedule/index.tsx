import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LaptopPickupSchedule from './laptop-pickup-schedule';
import LaptopPickupScheduleDetail from './laptop-pickup-schedule-detail';
import LaptopPickupScheduleUpdate from './laptop-pickup-schedule-update';
import LaptopPickupScheduleDeleteDialog from './laptop-pickup-schedule-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LaptopPickupScheduleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LaptopPickupScheduleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LaptopPickupScheduleDetail} />
      <ErrorBoundaryRoute path={match.url} component={LaptopPickupSchedule} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LaptopPickupScheduleDeleteDialog} />
  </>
);

export default Routes;
