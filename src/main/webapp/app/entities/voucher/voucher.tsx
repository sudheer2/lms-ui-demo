import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './voucher.reducer';
import { IVoucher } from 'app/shared/model/voucher.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVoucherProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Voucher = (props: IVoucherProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { voucherList, match, loading } = props;
  return (
    <div>
      <h2 id="voucher-heading">
        Vouchers
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Voucher
        </Link>
      </h2>
      <div className="table-responsive">
        {voucherList && voucherList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Voucher Code</th>
                <th>Voucher Name</th>
                <th>Assigned</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {voucherList.map((voucher, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${voucher.id}`} color="link" size="sm">
                      {voucher.id}
                    </Button>
                  </td>
                  <td>{voucher.voucherCode}</td>
                  <td>{voucher.voucherName}</td>
                  <td>{voucher.assigned ? 'true' : 'false'}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${voucher.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${voucher.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${voucher.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Vouchers found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ voucher }: IRootState) => ({
  voucherList: voucher.entities,
  loading: voucher.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Voucher);
