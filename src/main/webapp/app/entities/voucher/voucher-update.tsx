import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './voucher.reducer';
import { IVoucher } from 'app/shared/model/voucher.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IVoucherUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherUpdate = (props: IVoucherUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { voucherEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/voucher');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...voucherEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.voucher.home.createOrEditLabel">Create or edit a Voucher</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : voucherEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="voucher-id">ID</Label>
                  <AvInput id="voucher-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="voucherCodeLabel" for="voucher-voucherCode">
                  Voucher Code
                </Label>
                <AvField
                  id="voucher-voucherCode"
                  type="text"
                  name="voucherCode"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="voucherNameLabel" for="voucher-voucherName">
                  Voucher Name
                </Label>
                <AvField id="voucher-voucherName" type="text" name="voucherName" />
              </AvGroup>
              <AvGroup check>
                <Label id="assignedLabel">
                  <AvInput id="voucher-assigned" type="checkbox" className="form-check-input" name="assigned" />
                  Assigned
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/voucher" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  voucherEntity: storeState.voucher.entity,
  loading: storeState.voucher.loading,
  updating: storeState.voucher.updating,
  updateSuccess: storeState.voucher.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherUpdate);
