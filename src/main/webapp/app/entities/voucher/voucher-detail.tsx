import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './voucher.reducer';
import { IVoucher } from 'app/shared/model/voucher.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVoucherDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherDetail = (props: IVoucherDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { voucherEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Voucher [<b>{voucherEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="voucherCode">Voucher Code</span>
          </dt>
          <dd>{voucherEntity.voucherCode}</dd>
          <dt>
            <span id="voucherName">Voucher Name</span>
          </dt>
          <dd>{voucherEntity.voucherName}</dd>
          <dt>
            <span id="assigned">Assigned</span>
          </dt>
          <dd>{voucherEntity.assigned ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/voucher" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/voucher/${voucherEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ voucher }: IRootState) => ({
  voucherEntity: voucher.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherDetail);
