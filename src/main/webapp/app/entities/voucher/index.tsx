import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Voucher from './voucher';
import VoucherDetail from './voucher-detail';
import VoucherUpdate from './voucher-update';
import VoucherDeleteDialog from './voucher-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={VoucherUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={VoucherUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={VoucherDetail} />
      <ErrorBoundaryRoute path={match.url} component={Voucher} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={VoucherDeleteDialog} />
  </>
);

export default Routes;
