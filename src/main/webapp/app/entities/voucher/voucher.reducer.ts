import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IVoucher, defaultValue } from 'app/shared/model/voucher.model';

export const ACTION_TYPES = {
  FETCH_VOUCHER_LIST: 'voucher/FETCH_VOUCHER_LIST',
  FETCH_VOUCHER: 'voucher/FETCH_VOUCHER',
  CREATE_VOUCHER: 'voucher/CREATE_VOUCHER',
  UPDATE_VOUCHER: 'voucher/UPDATE_VOUCHER',
  DELETE_VOUCHER: 'voucher/DELETE_VOUCHER',
  RESET: 'voucher/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IVoucher>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type VoucherState = Readonly<typeof initialState>;

// Reducer

export default (state: VoucherState = initialState, action): VoucherState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_VOUCHER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_VOUCHER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_VOUCHER):
    case REQUEST(ACTION_TYPES.UPDATE_VOUCHER):
    case REQUEST(ACTION_TYPES.DELETE_VOUCHER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_VOUCHER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_VOUCHER):
    case FAILURE(ACTION_TYPES.CREATE_VOUCHER):
    case FAILURE(ACTION_TYPES.UPDATE_VOUCHER):
    case FAILURE(ACTION_TYPES.DELETE_VOUCHER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_VOUCHER):
    case SUCCESS(ACTION_TYPES.UPDATE_VOUCHER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_VOUCHER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/vouchers';

// Actions

export const getEntities: ICrudGetAllAction<IVoucher> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_VOUCHER_LIST,
  payload: axios.get<IVoucher>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IVoucher> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_VOUCHER,
    payload: axios.get<IVoucher>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IVoucher> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_VOUCHER,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IVoucher> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_VOUCHER,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IVoucher> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_VOUCHER,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
