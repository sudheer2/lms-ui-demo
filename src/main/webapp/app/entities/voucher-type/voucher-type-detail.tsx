import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './voucher-type.reducer';
import { IVoucherType } from 'app/shared/model/voucher-type.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IVoucherTypeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherTypeDetail = (props: IVoucherTypeDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { voucherTypeEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          VoucherType [<b>{voucherTypeEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="voucherCategory">Voucher Category</span>
          </dt>
          <dd>{voucherTypeEntity.voucherCategory}</dd>
          <dt>Voucher</dt>
          <dd>{voucherTypeEntity.voucher ? voucherTypeEntity.voucher.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/voucher-type" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/voucher-type/${voucherTypeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ voucherType }: IRootState) => ({
  voucherTypeEntity: voucherType.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherTypeDetail);
