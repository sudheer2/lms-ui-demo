import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IVoucher } from 'app/shared/model/voucher.model';
import { getEntities as getVouchers } from 'app/entities/voucher/voucher.reducer';
import { getEntity, updateEntity, createEntity, reset } from './voucher-type.reducer';
import { IVoucherType } from 'app/shared/model/voucher-type.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IVoucherTypeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherTypeUpdate = (props: IVoucherTypeUpdateProps) => {
  const [voucherId, setVoucherId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { voucherTypeEntity, vouchers, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/voucher-type');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getVouchers();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...voucherTypeEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.voucherType.home.createOrEditLabel">Create or edit a VoucherType</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : voucherTypeEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="voucher-type-id">ID</Label>
                  <AvInput id="voucher-type-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="voucherCategoryLabel" for="voucher-type-voucherCategory">
                  Voucher Category
                </Label>
                <AvInput
                  id="voucher-type-voucherCategory"
                  type="select"
                  className="form-control"
                  name="voucherCategory"
                  value={(!isNew && voucherTypeEntity.voucherCategory) || 'DIGITAL_CHALK'}
                >
                  <option value="DIGITAL_CHALK">DIGITAL_CHALK</option>
                  <option value="EBOOK1">EBOOK1</option>
                  <option value="EBOOK2">EBOOK2</option>
                  <option value="LAB">LAB</option>
                  <option value="EXAM_PREP">EXAM_PREP</option>
                  <option value="EXAM">EXAM</option>
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="voucher-type-voucher">Voucher</Label>
                <AvInput id="voucher-type-voucher" type="select" className="form-control" name="voucher.id">
                  <option value="" key="0" />
                  {vouchers
                    ? vouchers.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/voucher-type" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  vouchers: storeState.voucher.entities,
  voucherTypeEntity: storeState.voucherType.entity,
  loading: storeState.voucherType.loading,
  updating: storeState.voucherType.updating,
  updateSuccess: storeState.voucherType.updateSuccess,
});

const mapDispatchToProps = {
  getVouchers,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherTypeUpdate);
