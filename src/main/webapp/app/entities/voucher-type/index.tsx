import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import VoucherType from './voucher-type';
import VoucherTypeDetail from './voucher-type-detail';
import VoucherTypeUpdate from './voucher-type-update';
import VoucherTypeDeleteDialog from './voucher-type-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={VoucherTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={VoucherTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={VoucherTypeDetail} />
      <ErrorBoundaryRoute path={match.url} component={VoucherType} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={VoucherTypeDeleteDialog} />
  </>
);

export default Routes;
