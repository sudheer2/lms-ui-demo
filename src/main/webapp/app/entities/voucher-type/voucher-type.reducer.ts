import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IVoucherType, defaultValue } from 'app/shared/model/voucher-type.model';

export const ACTION_TYPES = {
  FETCH_VOUCHERTYPE_LIST: 'voucherType/FETCH_VOUCHERTYPE_LIST',
  FETCH_VOUCHERTYPE: 'voucherType/FETCH_VOUCHERTYPE',
  CREATE_VOUCHERTYPE: 'voucherType/CREATE_VOUCHERTYPE',
  UPDATE_VOUCHERTYPE: 'voucherType/UPDATE_VOUCHERTYPE',
  DELETE_VOUCHERTYPE: 'voucherType/DELETE_VOUCHERTYPE',
  RESET: 'voucherType/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IVoucherType>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type VoucherTypeState = Readonly<typeof initialState>;

// Reducer

export default (state: VoucherTypeState = initialState, action): VoucherTypeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERTYPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_VOUCHERTYPE):
    case REQUEST(ACTION_TYPES.UPDATE_VOUCHERTYPE):
    case REQUEST(ACTION_TYPES.DELETE_VOUCHERTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERTYPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERTYPE):
    case FAILURE(ACTION_TYPES.CREATE_VOUCHERTYPE):
    case FAILURE(ACTION_TYPES.UPDATE_VOUCHERTYPE):
    case FAILURE(ACTION_TYPES.DELETE_VOUCHERTYPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERTYPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERTYPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_VOUCHERTYPE):
    case SUCCESS(ACTION_TYPES.UPDATE_VOUCHERTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_VOUCHERTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/voucher-types';

// Actions

export const getEntities: ICrudGetAllAction<IVoucherType> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_VOUCHERTYPE_LIST,
  payload: axios.get<IVoucherType>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IVoucherType> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_VOUCHERTYPE,
    payload: axios.get<IVoucherType>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IVoucherType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_VOUCHERTYPE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IVoucherType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_VOUCHERTYPE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IVoucherType> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_VOUCHERTYPE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
