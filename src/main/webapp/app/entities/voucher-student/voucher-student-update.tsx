import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IVoucher } from 'app/shared/model/voucher.model';
import { getEntities as getVouchers } from 'app/entities/voucher/voucher.reducer';
import { ILaptopStudent } from 'app/shared/model/laptop-student.model';
import { getEntities as getLaptopStudents } from 'app/entities/laptop-student/laptop-student.reducer';
import { getEntity, updateEntity, createEntity, reset } from './voucher-student.reducer';
import { IVoucherStudent } from 'app/shared/model/voucher-student.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IVoucherStudentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const VoucherStudentUpdate = (props: IVoucherStudentUpdateProps) => {
  const [voucherId, setVoucherId] = useState('0');
  const [laptopStudentId, setLaptopStudentId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { voucherStudentEntity, vouchers, laptopStudents, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/voucher-student');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getVouchers();
    props.getLaptopStudents();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...voucherStudentEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="trainsLmsUiApp.voucherStudent.home.createOrEditLabel">Create or edit a VoucherStudent</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : voucherStudentEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="voucher-student-id">ID</Label>
                  <AvInput id="voucher-student-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label for="voucher-student-voucher">Voucher</Label>
                <AvInput id="voucher-student-voucher" type="select" className="form-control" name="voucher.id">
                  <option value="" key="0" />
                  {vouchers
                    ? vouchers.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <AvGroup>
                <Label for="voucher-student-laptopStudent">Laptop Student</Label>
                <AvInput id="voucher-student-laptopStudent" type="select" className="form-control" name="laptopStudent.id">
                  <option value="" key="0" />
                  {laptopStudents
                    ? laptopStudents.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/voucher-student" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  vouchers: storeState.voucher.entities,
  laptopStudents: storeState.laptopStudent.entities,
  voucherStudentEntity: storeState.voucherStudent.entity,
  loading: storeState.voucherStudent.loading,
  updating: storeState.voucherStudent.updating,
  updateSuccess: storeState.voucherStudent.updateSuccess,
});

const mapDispatchToProps = {
  getVouchers,
  getLaptopStudents,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(VoucherStudentUpdate);
