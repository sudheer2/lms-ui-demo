import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import VoucherStudent from './voucher-student';
import VoucherStudentDetail from './voucher-student-detail';
import VoucherStudentUpdate from './voucher-student-update';
import VoucherStudentDeleteDialog from './voucher-student-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={VoucherStudentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={VoucherStudentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={VoucherStudentDetail} />
      <ErrorBoundaryRoute path={match.url} component={VoucherStudent} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={VoucherStudentDeleteDialog} />
  </>
);

export default Routes;
