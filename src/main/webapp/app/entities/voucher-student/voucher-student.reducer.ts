import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IVoucherStudent, defaultValue } from 'app/shared/model/voucher-student.model';

export const ACTION_TYPES = {
  FETCH_VOUCHERSTUDENT_LIST: 'voucherStudent/FETCH_VOUCHERSTUDENT_LIST',
  FETCH_VOUCHERSTUDENT: 'voucherStudent/FETCH_VOUCHERSTUDENT',
  CREATE_VOUCHERSTUDENT: 'voucherStudent/CREATE_VOUCHERSTUDENT',
  UPDATE_VOUCHERSTUDENT: 'voucherStudent/UPDATE_VOUCHERSTUDENT',
  DELETE_VOUCHERSTUDENT: 'voucherStudent/DELETE_VOUCHERSTUDENT',
  RESET: 'voucherStudent/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IVoucherStudent>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type VoucherStudentState = Readonly<typeof initialState>;

// Reducer

export default (state: VoucherStudentState = initialState, action): VoucherStudentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERSTUDENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_VOUCHERSTUDENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_VOUCHERSTUDENT):
    case REQUEST(ACTION_TYPES.UPDATE_VOUCHERSTUDENT):
    case REQUEST(ACTION_TYPES.DELETE_VOUCHERSTUDENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERSTUDENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_VOUCHERSTUDENT):
    case FAILURE(ACTION_TYPES.CREATE_VOUCHERSTUDENT):
    case FAILURE(ACTION_TYPES.UPDATE_VOUCHERSTUDENT):
    case FAILURE(ACTION_TYPES.DELETE_VOUCHERSTUDENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERSTUDENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_VOUCHERSTUDENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_VOUCHERSTUDENT):
    case SUCCESS(ACTION_TYPES.UPDATE_VOUCHERSTUDENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_VOUCHERSTUDENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/voucher-students';

// Actions

export const getEntities: ICrudGetAllAction<IVoucherStudent> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_VOUCHERSTUDENT_LIST,
  payload: axios.get<IVoucherStudent>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IVoucherStudent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_VOUCHERSTUDENT,
    payload: axios.get<IVoucherStudent>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IVoucherStudent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_VOUCHERSTUDENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IVoucherStudent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_VOUCHERSTUDENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IVoucherStudent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_VOUCHERSTUDENT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
