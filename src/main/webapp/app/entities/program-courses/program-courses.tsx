import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-courses.reducer';
import { IProgramCourses } from 'app/shared/model/program-courses.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCoursesProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ProgramCourses = (props: IProgramCoursesProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { programCoursesList, match, loading } = props;
  return (
    <div>
      <h2 id="program-courses-heading">
        Program Courses
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Program Courses
        </Link>
      </h2>
      <div className="table-responsive">
        {programCoursesList && programCoursesList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Program</th>
                <th>Course</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programCoursesList.map((programCourses, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programCourses.id}`} color="link" size="sm">
                      {programCourses.id}
                    </Button>
                  </td>
                  <td>
                    {programCourses.program ? <Link to={`program/${programCourses.program.id}`}>{programCourses.program.id}</Link> : ''}
                  </td>
                  <td>{programCourses.course ? <Link to={`course/${programCourses.course.id}`}>{programCourses.course.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programCourses.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programCourses.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programCourses.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Program Courses found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ programCourses }: IRootState) => ({
  programCoursesList: programCourses.entities,
  loading: programCourses.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProgramCourses);
