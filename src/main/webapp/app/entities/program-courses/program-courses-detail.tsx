import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-courses.reducer';
import { IProgramCourses } from 'app/shared/model/program-courses.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCoursesDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProgramCoursesDetail = (props: IProgramCoursesDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { programCoursesEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          ProgramCourses [<b>{programCoursesEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>Program</dt>
          <dd>{programCoursesEntity.program ? programCoursesEntity.program.id : ''}</dd>
          <dt>Course</dt>
          <dd>{programCoursesEntity.course ? programCoursesEntity.course.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/program-courses" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/program-courses/${programCoursesEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ programCourses }: IRootState) => ({
  programCoursesEntity: programCourses.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProgramCoursesDetail);
