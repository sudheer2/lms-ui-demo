import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCourses, defaultValue } from 'app/shared/model/program-courses.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOURSES_LIST: 'programCourses/FETCH_PROGRAMCOURSES_LIST',
  FETCH_PROGRAMCOURSES: 'programCourses/FETCH_PROGRAMCOURSES',
  CREATE_PROGRAMCOURSES: 'programCourses/CREATE_PROGRAMCOURSES',
  UPDATE_PROGRAMCOURSES: 'programCourses/UPDATE_PROGRAMCOURSES',
  DELETE_PROGRAMCOURSES: 'programCourses/DELETE_PROGRAMCOURSES',
  RESET: 'programCourses/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCourses>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ProgramCoursesState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCoursesState = initialState, action): ProgramCoursesState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOURSES_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOURSES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOURSES):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOURSES):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOURSES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOURSES_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOURSES):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOURSES):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOURSES):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOURSES):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOURSES_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOURSES):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOURSES):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOURSES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOURSES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-courses';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCourses> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PROGRAMCOURSES_LIST,
  payload: axios.get<IProgramCourses>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IProgramCourses> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOURSES,
    payload: axios.get<IProgramCourses>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProgramCourses> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOURSES,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCourses> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOURSES,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCourses> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOURSES,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
