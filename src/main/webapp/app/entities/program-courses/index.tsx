import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCourses from './program-courses';
import ProgramCoursesDetail from './program-courses-detail';
import ProgramCoursesUpdate from './program-courses-update';
import ProgramCoursesDeleteDialog from './program-courses-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCoursesUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCoursesUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCoursesDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCourses} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ProgramCoursesDeleteDialog} />
  </>
);

export default Routes;
